<?php

/**
 * Fired during plugin activation
 *
 * @link       https://hyperclock.eu
 * @since      1.0.0
 *
 * @package    Tut_Exchange
 * @subpackage Tut_Exchange/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Tut_Exchange
 * @subpackage Tut_Exchange/includes
 * @author     JMColeman (hyperclock) <hpk@hyperclock.eu>
 */
class Tut_Exchange_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
