<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://hyperclock.eu
 * @since      1.0.0
 *
 * @package    Tut_Exchange
 * @subpackage Tut_Exchange/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Tut_Exchange
 * @subpackage Tut_Exchange/includes
 * @author     JMColeman (hyperclock) <hpk@hyperclock.eu>
 */
class Tut_Exchange_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'tut-exchange',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
